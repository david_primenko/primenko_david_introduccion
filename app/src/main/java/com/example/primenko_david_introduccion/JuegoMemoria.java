package com.example.primenko_david_introduccion;

import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by dprimenko on 6/10/16.
 */

public class JuegoMemoria {

    private Drawable imgTemporal;
    private int aciertos;
    private TypedArray aImagenes;
    private ArrayList<ImageButton> btnAcertados;

    //region Getter & Setter
    public int getAciertos() {
        return aciertos;
    }
    public void setAciertos(int aciertos) {
        this.aciertos = aciertos;
    }
    public Drawable getImgTemporal() {
        return imgTemporal;
    }
    public ArrayList<ImageButton> getBtnAcertados() {
        return btnAcertados;
    }
    public void setBtnAcertados(ArrayList<ImageButton> btnAcertados) {
        this.btnAcertados = btnAcertados;
    }
    public void setImgTemporal(Drawable imgTemporal) {
        this.imgTemporal = imgTemporal;
    }
    //endregion

    public JuegoMemoria(TypedArray aImagenes) {
        aciertos = 0;
        this.aImagenes = aImagenes;
        btnAcertados = new ArrayList<>();
    }

    public ArrayList<Drawable> RellenarHashtable() {

        ArrayList<Drawable> arrayBtnImg = new ArrayList<>();
        for (int i = 0; i < aImagenes.length(); i++){
            arrayBtnImg.add(aImagenes.getDrawable(i));
        }
        return arrayBtnImg;
    }

    public boolean ComprobarSimilitud(Drawable img) {

        boolean respuesta = false;

        if (img.getConstantState().toString().equalsIgnoreCase(imgTemporal.getConstantState().toString())) {

            aciertos += 1;
            respuesta = true;
        }

        return respuesta;
    }

    public boolean ComprobarJuegoAcabado() {
        boolean respuesta = false;

        if(aciertos == 10) {
            respuesta = true;
        }

        return respuesta;
    }

    public void addBotonAcertado(ImageButton btn) {
        if (btn != null) {
            btnAcertados.add(btn);
        }
    }

}
