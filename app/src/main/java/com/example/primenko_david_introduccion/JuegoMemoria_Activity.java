package com.example.primenko_david_introduccion;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

public class JuegoMemoria_Activity extends AppCompatActivity {

    private int cartasLevantadas;
    private ImageButton btnTemporal;
    private ArrayList<Drawable> arrayBtnImg;
    private JuegoMemoria juego;
    private Drawable cartaLevantada;
    private static final long PAUSA = 1;
    private static final long INTERVALO = 1000;
    private MyCountDownTimer contadorPausa;
    private TableLayout tabla;
    private TableRow fila;
    private View boton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego_memoria);
        arrayBtnImg = new ArrayList<>();
        cartasLevantadas = 0;
        IniciarJuego();

    }
    public void darVuelta (View v) {

        ImageButton btn = (ImageButton)v;
        cartasLevantadas += 1;

        if (cartasLevantadas < 2) {
            btnTemporal = btn;
            cartaLevantada = arrayBtnImg.get(Integer.parseInt(btn.getTag().toString()));
            juego.setImgTemporal(cartaLevantada);
            btn.setImageDrawable(cartaLevantada);
            btn.setEnabled(false);
        }
        else {
            cartaLevantada = arrayBtnImg.get(Integer.parseInt(btn.getTag().toString()));
            btn.setImageDrawable(cartaLevantada);


            if (!(juego.ComprobarSimilitud(cartaLevantada))) {
                bloquearBotones();
                contadorPausa = new MyCountDownTimer((PAUSA * 750), INTERVALO, btn);
                contadorPausa.start();
            }
            else {
                btnTemporal.setEnabled(false);
                btn.setEnabled(false);
                juego.addBotonAcertado(btnTemporal);
                juego.addBotonAcertado(btn);

                if (juego.ComprobarJuegoAcabado()) {
                    //activarControlesGanador();

                    AlertDialog.Builder popup=new AlertDialog.Builder(this);
                    popup.setTitle("HA GANADO!");
                    popup.setMessage("¿Quieres volver a jugar?");
                    popup.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            reiniciarJuego(JuegoMemoria_Activity.this);
                        }
                    });
                    popup.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            JuegoMemoria_Activity.this.finish();
                        }
                    });
                    popup.show();
                }
            }
            cartasLevantadas = 0;
        }
    }

    public void IniciarJuego() {
        Random rnd = new Random();
        int numAleatorio = rnd.nextInt(5);
        TypedArray aImagenes;

        switch (numAleatorio) {
            case 1:
                aImagenes = getResources().obtainTypedArray(R.array.posicionDos);
                break;
            case 2:
                aImagenes = getResources().obtainTypedArray(R.array.posicionTres);
                break;
            case 3:
                aImagenes = getResources().obtainTypedArray(R.array.posicionCuatro);
                break;
            case 4:
                aImagenes = getResources().obtainTypedArray(R.array.posicionCinco);
                break;
            default:
                aImagenes = getResources().obtainTypedArray(R.array.posicionUno);
                break;
        }

        juego = new JuegoMemoria(aImagenes);
        arrayBtnImg = juego.RellenarHashtable();
    }

    public void bloquearBotones() {
        tabla = (TableLayout) findViewById(R.id.activity_juego_memoria);
        for (int i = 0; i < tabla.getChildCount(); i++) {
            fila = (TableRow) tabla.getChildAt(i);
            for (int j = 0; j < fila.getChildCount(); j++) {
                boton = (View) fila.getChildAt(j);
                if (!(juego.getBtnAcertados().contains(boton))) {
                    boton.setEnabled(false);
                }
            }
        }
    }

    public void desbloquearBotones() {
        tabla = (TableLayout) findViewById(R.id.activity_juego_memoria);
        for (int i = 0; i < tabla.getChildCount(); i++) {
            fila = (TableRow) tabla.getChildAt(i);
            for (int j = 0; j < fila.getChildCount(); j++) {
                boton = (View) fila.getChildAt(j);
                if (!(juego.getBtnAcertados().contains(boton))) {
                    boton.setEnabled(true);
                }
            }
        }
    }


    public static void reiniciarJuego(Activity actividad){
        Intent intent=new Intent();
        intent.setClass(actividad, actividad.getClass());
        //llamamos a la actividad
        actividad.startActivity(intent);
        //finalizamos la actividad actual
        actividad.finish();
    }

    public class MyCountDownTimer extends CountDownTimer {

        ImageButton btn;

        public MyCountDownTimer(long startTime, long interval, ImageButton btn) {
            super(startTime, interval);
            this.btn = btn;
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            btnTemporal.setEnabled(true);
            btnTemporal.setImageResource(R.mipmap.ic_launcher);
            btn.setImageResource(R.mipmap.ic_launcher);
            desbloquearBotones();
        }

    }
}
